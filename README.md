## Gerador de Senhas Aleatórias Seguro com Spring Boot

**Introdução:**

Este projeto implementa um gerador de senhas aleatórias robusto e seguro utilizando o framework Spring Boot. Ele oferece uma API RESTful para gerar senhas personalizadas de acordo com as necessidades do usuário.

**Funcionalidades:**

* Gera senhas aleatórias com caracteres personalizáveis (letras maiúsculas, minúsculas, números, símbolos).
* Permite definir o tamanho da senha.
* Integra-se com o Random.org para garantir a qualidade e aleatoriedade das senhas.
* Implementa tratamento de erros e validação de entrada.
* Retorna senhas no formato de texto simples.

**Benefícios:**

* **Segurança:** O uso do Random.org garante que as senhas sejam geradas de forma verdadeiramente aleatória e criptograficamente segura.
* **Facilidade de Uso:** A API RESTful torna a integração com outras aplicações simples e intuitiva.
* **Flexibilidade:** A personalização de caracteres e tamanho da senha permite que os usuários criem senhas que atendam às suas necessidades específicas.
* **Escalabilidade:** A arquitetura Spring Boot facilita a escalabilidade do aplicativo para lidar com um grande volume de solicitações.
* **Manutenção:** O código bem estruturado e documentado facilita a manutenção e aprimoramento do projeto.

**Tecnologias Utilizadas:**

* Spring Boot: Framework Java para desenvolvimento rápido e robusto de APIs RESTful.
* Jackson: Biblioteca JSON para serialização e deserialização de dados JSON.
* Lombok: Biblioteca para reduzir boilerplate de código (opcional).
* Random.org: Serviço online para geração de números e senhas aleatórias.

Obs: ARQUIVOS NA BRANCH MASTER
